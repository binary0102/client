import React from 'react';
import {render} from 'react-dom';
import "./css/custom.scss"
import "bootstrap/scss/bootstrap.scss"
import Home from './containers/Home';
render(<Home />, document.getElementById('app'));
