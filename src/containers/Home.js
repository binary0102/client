import React,{ Component, Fragment } from 'react'
import Header from '../components/Header';
import Category from '../components/Category';
import Content from '../components/Content';
import Footer from '../components/Footer';
import Section from '../components/Section';
export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return ( 
            <Fragment>
                <Header />
                <Section />
                <div className="container">
                    <div className="row">
                    <Category />
                    <Content />
                   
                    </div>
                </div>
                
                <Footer />    
            </Fragment>
        );
    }
}