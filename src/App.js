import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader'
const FIELDS = 'email,name,picture';
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
        };
    }
    componentDidMount() {
        window.fbAsyncInit = function() {
            FB.init({
              appId            : '295939674394070',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : 'v3.2'
            });
          };
        
          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
    } 
    
    login = () => {
        window.FB.login(response => {
            this.statusChangeCallBack(response);
        },{scope: 'email,public_profile'});
    }
    statusChangeCallBack(response) {
        console.log("Status Change CallBack", response.authResponse.accessToken);

        if(response.status === 'connected') {
            console.log("Success");
            this.apiCall();
        }else if (response.status === 'not_authorized') {
            console.log("Login into facebook , but not the app ");
        }else {
            console.log("Not logged into Facebook or other");
        }
    }
    apiCall() {
        window.FB.api(`/me?fields=${FIELDS}`, function(res) {
            console.log(JSON.stringify(res));
            console.log("Logged in as ", res);
            
            this.setState({
                user: res,
            })
            console.log(this.state.user);
        }.bind(this));
    }
    showInfoUser = () => {
        console.log(this.state.user);
    }
    render() {
       return (
        <React.Fragment>
            <button onClick={this.login}> Login Facebook </button>
            <button onClick={this.showInfoUser}>Show info </button>
        </React.Fragment>        
        );
        
    }
}
  
export default hot(module)(App)
