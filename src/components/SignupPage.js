import React, { Component } from 'react';
import SingupForm from './SingupForm';

export default class SignupPage extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <SingupForm />
                </div>
            </div> 
        );
    }
}