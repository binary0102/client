import React, { Component } from 'react';
import "../css/category.scss"
export default class Category extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
           <div className="col-lg-3">
                <div className="aside-item">
                    <h2 className="title-head">
                        <span>Thương hiệu</span>
                     </h2>
                </div>
                <div className="aside-content"> 
                    <div className="field-search form-group">
                        <input type="text" className="form-control" placeholder="Lọc theo thương hiệu"/>
					        <button className="btn btn-default btn-search"><i className="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                    <ul className="list-item">
                        <li><a href="#">Kingston <span>(40)</span> </a></li>
                        <li><a href="#">Lexar <span>(40)</span></a> </li>
                        <li><a href="#">Moment Semiconductor <span>(40)</span></a></li>
                        <li><a href="#">Samsung <span>(40)</span></a> </li>
                        <li><a href="#">Sandisk <span>(40)</span></a> </li>
                        <li><a href="#">Toshiba <span>(40)</span></a></li>
                    </ul>
                </div>
           </div>
        );
    }
}