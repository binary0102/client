import React,{ Component } from 'react';

export default class SignupForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
           
        }
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value }); 
    }

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }
    render() {
        return (
            <form onSubmit={this.onSubmit}> 
                <h1>Join our </h1>

                <div className="form-group">
                    <label className="control-label">Email</label>
                     <input
                        value={this.state.email}
                        onChange={this.onChange}
                        type="text"
                        name="email"
                        className="form-control" 
                     />
                     <label className="control-label">UserName</label>
                     <input
                        value={this.state.username}
                        onChange={this.onChange}
                        type="text"
                        name="username"
                        className="form-control" 
                     />
                     <label className="control-label">Password</label>
                     <input
                        value={this.state.password}
                        onChange={this.onChange}
                        type="password"
                        name="password"
                        className="form-control" 
                     />
                   
                </div> 
                <div className="form-group">
                    <button className="btn btn-primary btn-lg">
                        Sign up
                    </button>
                </div>
            </form>
        );
    }
}