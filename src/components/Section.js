import React, { Component } from 'react';
import "../css/section.scss";
export default class Section extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="section">
                    <div className="container"> 
                        <div className="row">
                            <div className="col-lg-12">
                                <ul>
                                    <li>
                                        <a href="#"><span>Trang chủ	</span></a>
                                        <span><i className="fa fa-angle-right" aria-hidden="true"></i></span>
                                    </li>
                                    <li>
                                        <strong itemprop="title"> MicroSD</strong>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        );
    }   
}