import React, { Component } from 'react';
import Product from '../images/product.jpg'
import '../css/product.scss';
import ContentLoader from "react-content-loader";
import axios from 'axios';
const API = 'http://localhost:4000/graphql';
const DEFAULT_QUERY = "{products {_id name price price_discount discount}}";

export default class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoading: false,
            error: null,
        };
    }
    componentDidMount() {
        this.setState({ isLoading: true });
        fetch(API , {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: "{products {_id name price price_discount discount}}"
            })
          })
            .then(response => {
                if (response.ok) {
                  return response.json();
                } else {
                  throw new Error('Something went wrong ...');
                }
            })
            .then(data => this.setState({products: data.data.products, isLoading: false}))
            .catch(error => this.setState({ error, isLoading: false }));
    }
    render() {
        const { products , isLoading} = this.state;
        if (isLoading) {
            return <ContentLoader />
          }
      
        return (
            <div className="col-lg-9">
                <div className="row">
                    <div className="col-xs-12 col-sm-6 col-md-6">
                        <h3>Top SSd</h3>
                    </div>
                </div>
                <div className="row">
                    {products.map(product => 
                         <div className="col-xs-6 col-sm-4 col-md-3 col-lg-3" key={product._id}>
                           <a href="#">
                         <div>
                                <img src={Product}></img>
                         </div>
                         <div className="product-info">
                            <h3> {product.name} </h3>
                         <div className="price">
                                <strong className="product-price"> {product.price_discount}₫</strong>
                                <span>{product.price}₫</span> 
                         </div>      
                         <label className="discount"> Giảm {product.discount}%</label>
                         <div className="rating">
                                <span className="fa fa-star checked"></span>
                                <span className="fa fa-star checked"></span>
                                <span className="fa fa-star checked"></span>
                                <span className="fa fa-star checked"></span>
                                <span className="fa fa-star checked"></span>
                                <span className="rating-result">15 đánh giá</span>
                          </div>
                          </div>
                         </a>
                    </div>
                    )}
                    
                </div>
                <div>
                    <nav aria-label="Page navigation example">
                        <ul className="pagination">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            
        );
    }
}

const MyLoader = props => (
	<ContentLoader 
		rtl
		height={160}
		width={399}
		speed={2}
		primaryColor="#f3f3f3"
		secondaryColor="#ecebeb"
		{...props}
	>
		<rect x="120.41" y="25.67" rx="0" ry="0" width="121.19" height="77" /> 
		<rect x="5.41" y="-0.33" rx="0" ry="0" width="587.86" height="290" />
	</ContentLoader>
)