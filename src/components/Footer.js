import React, { Component } from 'react';
import '../css/footer.scss';
import Boct from '../images/boct_1.png';
import Boct_1 from '../images/boct_2.png';
export default class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
          
            <footer className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-21 col-lg-4">
                            <div className="wiget">
                                <div className="title-menu">Chăm sóc khách hàng</div>
                                <ul className="list-menu">
									
									<li><a href="#">Hướng dẫn mua hàng</a></li>
									
									<li><a href="about-memoryzone">Vận chuyển</a></li>
									
									<li><a href="dieu-khoan-mua-ban-hang-hoa">Điều khoản giao dịch</a></li>
									
									<li><a href="chinh-sach-bao-mat">Bảo mật thông tin</a></li>
									
									<li><a href="memoryzone-tuyen-dung-nhieu-vi-tri">Tuyển dụng</a></li>
									
								</ul>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-21 col-lg-4">
                        <div className="wiget">
                            <div className="title-menu">CHÍNH SÁCH CÔNG TY</div>
                                <ul className="list-menu">
									
									<li><a href="#">Tuyển Dụng</a></li>
									
									<li><a href="about-memoryzone">Chính Sách Bảo Mật</a></li>
									
									<li><a href="dieu-khoan-mua-ban-hang-hoa">Chính Hãng</a></li>
									
									<li><a href="chinh-sach-bao-mat">Bảo mật thông tin</a></li>
									
									<li><a href="memoryzone-tuyen-dung-nhieu-vi-tri">Tuyển dụng</a></li>
									
								</ul>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-21 col-lg-4">
                             <div className="wiget">
                                <div className="title-menu">HỖ TRỢ KHÁCH HÀNG</div>
                                <ul className="list-menu">
									
									<li><a href="#"><i className="fa fa-facebook-official  fa-2x"></i> Facebook</a></li>
									
									<li><a href="about-memoryzone">Trả Hàng & Hoàn Tiền</a></li>
									
									<li><a href="dieu-khoan-mua-ban-hang-hoa">Chăm Sóc Khách Hàng</a></li>
									
									<li><a href="chinh-sach-bao-mat">Chính Sách Bảo Hành</a></li>
									<li>
                                        <img src={Boct}/>
                                        <img src={Boct_1}/>
                                        
                                    </li>
								</ul>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </footer>
        );
    }
}