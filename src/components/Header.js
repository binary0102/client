import React, { PropTypes, Component } from 'react'
import '../css/header.scss'
import logo from '../images/logo.png'
export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <header>           
                    <div className="wrap_main">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3">
                                    <div className="title_menu">
                                        <span className="title_">Danh mục sản phẩm</span>
                                        <span className="nav_button"><span><i class="fa fa-bars" aria-hidden="true"></i></span></span>
                                    </div>
                                </div>
                                <div className="col-lg-9">
                                     <div className="row">
                                        <nav class="navbar navbar-expand-lg ">
                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                                <ul class="navbar-nav mr-auto">
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">TRANG CHỦ <span class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">THANH TOÁN <span class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">CHÍNH SÁCH GIAO HÀNG<span class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">LIÊN HỆ   <span class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">XGEAR - BY MEMORYZONE MỚI <span class="sr-only">(current)</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </header>
        ); 
    }
}